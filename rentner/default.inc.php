<?php
/**
 * $Id: default.inc.php,v 1.8 2007-12-30 17:37:56 tolimar Exp $
 * $Revision: 1.8 $
 * $Author: tolimar $
 * $Date: 2007-12-30 17:37:56 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.8 $');
define('SECTION_AUTHOR', '$Author: tolimar $');
?>
<font size="+1"><b>Willkommen auf der Rentner Seite des #debian.de (IRCnet)
Channels</b></font><br><br>

Hier findet ihr allgemeine Informationen &uuml;ber Channel und Leute. Solltet 
ihr euch die Frage stellen: "Wieso Rentner?", dann lasst mich euch dies kurz 
erl&auml;utern:<br>
Es gab einmal eine Zeit im Channel die war nicht friedlich sondern im Gegenteil 
sehr chaotisch und anstrengend. Channeloperatoren (Ops) die sich gegenseitig anmachten und 
Ops in Massen. Dies ging am Ende soweit das sich Ops nicht einmal mehr untereinander kannten.
Das f&uuml;hrte nat&uuml;rlich irgendwann zu Konflikten im Channel, da Fragen aufkamen wie: "Warum opst 
du Person XY??". Es kam zu Gr&uuml;ppchenbildungen  und das <a href="http://www.infodrom.org/Debian/support/debian.de/opwars.html">Klima auf dem Channel war vergiftet</a>,
vor allen Dingen waren sich die Ops nicht &uuml;ber Regeln auf dem Channel einig. 
Irgendwann hatten einige die Nase voll, verlie&szlig;en den Channel und suchten woanders
Zuflucht. Dies sind zu einem guten Teil die Leute die heute Rentner heissen...
Der Name Rentner entstand aus der Haltung sich von andern Gruppen zu distanzieren. 
Ja und heute ? Wir sind jetzt 20 satte Rentner, die sich alle untereinander kennen, sei es
aus dem Reallife oder aus dem IRC. Wir tauschen unsere Meinungen untereinander aus, 
was in einem so bekannten und gro&szlig;en Channel wie #debian.de auch notwendig ist. 
Uns gibt es nun 1 1/2 Jahre und dieser Schritt ist dem Channel zu Gute gekommen. 
Mit dieser Seite wollen wir zeigen das es uns gibt und das wir keine Au&szlig;erirdische 
Verschw&ouml;rung des industriellen, militaerischen Komplexes sind :). Ihr k&ouml;nnt uns auch einfach mal in #debian.de.rentner 
besuchen kommen (in unserem Alter freut man sich immer &uuml;ber Besuch *g*).
<br><br>
<b>Die Rentner</b><br>
