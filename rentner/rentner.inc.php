<?php
/**
 * $Id: rentner.inc.php,v 1.22 2007-04-20 09:21:11 alfie Exp $
 * $Revision: 1.22 $
 * $Author: alfie $
 * $Date: 2007-04-20 09:21:11 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.22 $');
define('SECTION_AUTHOR', '$Author: alfie $');

$link = mysql_connect("localhost", "atbs", "atbs");
mysql_select_db("atbs");
?>
<table>
<tr>
<td colspan="2">	
	<b>Rentner Infos:</b><br>
	<table border="1" width="100%">
	<tr>
	<td><b>#</b></td>
	<td><b>IRC Nickname</b></td>
	<td><b>Realname</b></td>
	<td><b>E-Mail</b></td>
	<td><b>PGP-KeyID</b></td>
	<td><b>Info</b></td>
	<td><b>Abuse Reports</b></td>
	</tr>
	<?php
	$query = "SELECT * FROM users WHERE status != 'INACTIVE' ORDER BY username";
	$result = mysql_query($query);
	$count = 0;
	while ($row = mysql_fetch_array($result)) {
		$count++;
		if ($row['abuse_reports'] == 'TRUE') {
		    $abuse_reports = 'J';
		} else {
		    $abuse_reports = 'N';
		}
		
		foreach ($row as $key => $value) {
			if (empty($value)) {
				$row[$key] = ' ';
			}
		}

		echo "<tr>\n";
		echo "<td>".$count."</td>\n";
		echo "<td>".$row['username']."</td>\n";
                echo "<td>";
                
		echo $row['homepage'] != ' ' ?
			"<a href=\"".$row['homepage']."\">".$row['realname']."</a>" : $row['realname'];
		echo "</td>\n";

		echo "<td>";
		echo $row['email'] != ' ' ?
			"<a href=\"mailto:".$row['email']."\">".$row['email']."</a>" : "";
		echo "</td>\n";

		echo "<td><a href=\"http://keyserver.noreply.org/pks/lookup?search=".$row['pgpkeyid']."&amp;fingerprint=on&amp;op=index\">".$row['pgpkeyid']."</td>\n";
		
		echo "<td>".$row['info']."</td>\n";
		echo "<td>".$abuse_reports."</td>\n";
		echo "</tr>\n";
	}
	?>
	</table>
</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
<td width="50%" valign="top">
	<b>Aktuelle Wahlergebnisse:</b>
	<table border="1">
	<tr>
	<td><b>Kandidat</b></td>
	<td><b>Pro</b></td>
	<td><b>#</b></td>
	<td><b>Neutral</b></td>
	<td><b>#</b></td>
	<td><b>Contra</b></td>
	<td><b>#</b></td>
	</tr>
	<?php
	$query = "SELECT MAX(id) FROM current_vote";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$currentvoteid = $row['MAX(id)'];
	
	// find all vote candicates
	$query = "SELECT whom FROM vote WHERE voteid='".$currentvoteid."'";
	$result = mysql_query($query);
	$votecandidates = array();
	while ($row = mysql_fetch_array($result)) {
		$votecandidates[] = $row['whom'];
	}
	
	foreach ($votecandidates as $value) {
		$candidate = $value;
	        $query = "SELECT who,value FROM op_votes WHERE id='".$currentvoteid."' AND whom='".$candidate."'";
		$result = mysql_query($query);
		
		$pro = 0;
		$neu = 0;
		$con = 0;
		$provoterstr = "";
		$neuvoterstr = "";
		$negvoterstr = "";
		while ($row = mysql_fetch_array($result)) {
			$value = $row['value'];
			if ($value == 'PRO') {
				$provoterstr .= $row['who']."<br>";
				$pro++;
			} else if ($value == 'NEU') {
				$neuvoterstr .= $row['who']."<br>";
				$neu++;
			} else if ($value == 'CON') {
				$negvoterstr .= $row['who']."<br>";
				$con++;
			}
		}
	
		echo "<tr>\n";
		echo "<td><b>".$candidate."</b></td>\n";
		echo "<td>".$provoterstr."</td>\n";
		echo "<td><b>".$pro."</b></td>\n";
		echo "<td>".$neuvoterstr."</td>\n";
		echo "<td><b>".$neu."</b></td>\n";
		echo "<td>".$negvoterstr."</td>\n";
		echo "<td><b>".$con."</b></td>\n";
		echo "</tr>\n";
	}
	?>
	</table>
</td>

<td>
	<b>Wahlberechtigte:</b><br>
	<table border="1">
	<tr>
	<td><b>#</b></td>
	<td><b>IRC Nickname</b></td>
	</tr>
	<?php
	$query = "SELECT username FROM users WHERE status != 'INACTIVE' ORDER BY username";
	$result = mysql_query($query);
	$count = 0;
	while ($row = mysql_fetch_array($result)) {
		$count++;
		echo "<tr>\n";
		echo "<td>".$count."</td>\n";
		echo "<td>".$row['username']."</td>\n";
		echo "</tr>\n";
	}
	?>
	</table>
	<br>
</td>
</tr>

</table>
